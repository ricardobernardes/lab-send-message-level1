package main

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"log"
	"os"
)

const (
	String = "String"
	EnvSnsTopic = "SNS_TOPIC"
	EnvQueue = "QUEUE"
	QueueFilter = "queue_filter"

)

type Message struct {
	Id       uint 	`json:"id"`
	Name     string `json:"name"`
	Age  	 uint 	`json:"age"`
}

func notify(payload []byte) error {

	sess := session.Must(session.NewSession())
	snsService := sns.New(sess)

	params := &sns.PublishInput{
		Message: aws.String(string(payload)),
		TopicArn: aws.String(os.Getenv(EnvSnsTopic)),
		MessageAttributes: map[string]*sns.MessageAttributeValue {
			QueueFilter: {
				DataType: aws.String(String),
				StringValue: aws.String(os.Getenv(EnvQueue)),
			},
		},
	}

	resp, err := snsService.Publish(params)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	fmt.Println(os.Getenv(EnvQueue))
	fmt.Println(resp.String())
	return  nil
}

func run(msg Message) error{

	payload, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	return notify(payload)
}

func main(){
	lambda.Start(run)
}
