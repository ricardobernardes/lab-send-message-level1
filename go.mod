module testericardo

go 1.12

require (
	bitbucket.org/tilix-dev/pkg_api_gateway v1.0.33
	github.com/aws/aws-lambda-go v1.10.0
	github.com/aws/aws-sdk-go v1.23.2
	github.com/awslabs/aws-lambda-go-api-proxy v0.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/unidoc/unidoc v2.2.0+incompatible
	golang.org/x/image v0.0.0-20190507092727-e4e5bf290fec // indirect
	golang.org/x/net v0.0.0-20190509222800-a4d6f7feada5 // indirect
	golang.org/x/sys v0.0.0-20190509141414-a5b02f93d862 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
